(function () {
  /**
   * Check and set a global guard variable.
   * If this content script is injected into the same page again,
   * it will do nothing next time.
   */
  if (window.ghForkHasRun) {
      return;
  }
  window.ghForkHasRun = true;
  
  // Find the section of the page to modify
  var box = document.querySelector('.d-sm-flex');
  var actionsBox = document.querySelector('div.flex-justify-end:nth-child(2)')

  // TODO Bail when the actionsBox doesn't exist

  // Find the elements to replicate
  var pullRequest = actionsBox.children[0].cloneNode(true);
  var compare = actionsBox.children[1].cloneNode(true);

  // Relabel the actions
  pullRequest.replaceChild(document.createTextNode('PR'), pullRequest.lastChild);
  compare.replaceChild(document.createTextNode('Cmp'), compare.lastChild);
  // Adjust the URLs
  var urlParts = pullRequest.getAttribute('href').split('/');
  var user = urlParts[1].toLowerCase();
  var branch = urlParts[urlParts.length-1];
  var space = urlParts.splice(0,3).join('/');

  // Special snowflake that uses a form of gitflow
  if (user == 'wildmeorg') {
    var defaultBranch = 'develop';
  } else {
    var defaultBranch = 'master';
  };

  var cmpUrl = [space, 'compare', defaultBranch + '...' + user + ':' + branch].join('/');
  var prUrl = cmpUrl + '?expand=1';
  pullRequest.setAttribute('href', prUrl);
  compare.setAttribute('href', cmpUrl);

  // Append them to the page
  actionsBox.appendChild(pullRequest);
  actionsBox.appendChild(compare);

  /* for (var child=actionsBox.firstChild; child!=null; child=child.nextSibling) {
    console.log(child);
  } */

})();
