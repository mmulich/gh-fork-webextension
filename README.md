
# Web Extension for GitHub forks

Adds links to the page to compare or pull request directly against the master branch of the current fork rather than the upstream. It simply short circuits a step of selecting the fork from the drop down for the current pages.

## Before
![](area-of-interest.png)
## After
![](area-links-added.png)


## HTML for that section of the page

```html
<div class="d-sm-flex Box mb-3 Box-body bg-gray-light">
    <div class="d-flex flex-auto">
        This branch is 18 commits ahead of OrgOrUser:master.
    </div>
    <div class="d-flex flex-justify-end mt-2 mt-sm-0 ml-sm-3">
        <a class="muted-link ml-3" href="/OrgOrUser/repo/pull/new/???????????????????????">
          <svg class="octicon octicon-git-pull-request" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="..."></path></svg>
          Pull request
        </a>
      <a class="muted-link ml-3" href="/OrgOrUser/repo/compare/????????????????????????????">
        <svg class="octicon octicon-file-diff" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="..."></path></svg>
        Compare
      </a>
    </div>
  </div>
```

## Integrating into Firefox

See [Firefox workflow overview](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Firefox_workflow_overview)

### Development

See [Getting started with web-ext](https://extensionworkshop.com/documentation/develop/getting-started-with-web-ext/)

Use `web-ext run` or the following...

Follow these instructions to whack and smash on this code:

1. Checkout this project
1. In Firefox goto: `about:debugging`
1. Select "Load Temporary Add-on..."
1. At the prompt, open the `manifest.json`

You should now be able to use the extension in your browser.

### Install

See public release at https://addons.mozilla.org/en-US/firefox/addon/github-forked-links-extension/
